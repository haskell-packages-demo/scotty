import safe Data.Monoid
    ( mconcat )
import safe Prelude
    ( ($)
    , IO
    )
import Web.Scotty
    ( get
    , html
    , pathParam
    , scotty
    )

main :: IO ()
main = scotty 8080 $
  get "/:word" $ do
    beam <- pathParam "word"
    html $ mconcat ["<h1>Scotty, ", beam, " me up!</h1>"]
