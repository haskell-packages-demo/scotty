# scotty

Web framework inspired by Ruby's Sinatra, using WAI and Warp. https://www.stackage.org/package/scotty

# Books
* *Practical Web Development with Haskell: Master the Essential Skills to Build Fast and Scalable Web Applications*
  2018-11 Ecky Putrady (Apress, Springer)
